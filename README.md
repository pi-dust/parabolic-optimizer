# Parabolic Optimizer 
This the companion repository of the article "Efficient Parabolic Optimisation Algorithm for adaptive VQE implementations".
Here we present additional data and figures as well as source code of the parabolic optimizer, as was implemented in python.

## Repository Structure
- The file "parabolic_minimize.py" contains the source code of the parabolic optimizer.
- The directory "data" cointains the results from the various simulations we run.
- The directory "figures" contains several visual comparisons between different optimization methods.

More specifically, we present data for $`HF`$, $`LiH`$, $`H_20`$ and $`BeH_2`$ at their equilibrium bond lengths, as well as for bond lengths 50% longer than the equilibrium ones.
In the case of equilibrium bond lengths, we also present data for the Nelder-Mead optimizer in addition to the COBYLA optimizer which is used as our main benchmark.


## Additional Information
Let us include here some additional data that were omitted but might be useful for some readers.

| Molecule                                      | E$`_{FCI}`$ (Hartree)| E$`_{HF}`$ (Hartree)|
| ---                                           | ---                  | ---                 |
| $`HF`$ @ $`0.995\mathring{A}`$                | -98.603302           | -98.570972          |
| $`LiH`$ @ $`1.546\mathring{A}`$               | -7.882761            | -7.863134           |
| $`H_20`$ @ $`1.028\mathring{A}, 96.9\degree`$ | -75.023290           | -74.962652          |
| $`BeH_2`$ @ $`1.316\mathring{A}`$             | -15.595247           | -15.560822          |
| $`HF`$ @ $`1.493\mathring{A}`$                | -98.520681           | -98.417526          |
| $`LiH`$ @ $`2.319\mathring{A}`$               | -7.836575            | -7.793457           |
| $`H_20`$ @ $`1.542\mathring{A}, 96.9\degree`$ | -74.861507           | -74.678890          |
| $`BeH_2`$ @ $`1.974\mathring{A}`$             | -15.453075           | -15.364897          |
