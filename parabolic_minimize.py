import copy
from cmath import isclose


def get_partial_derivative(func, ansatz_list, value_at_ansatz, index, dx):
    new_list = copy.deepcopy(ansatz_list)
    new_list[index] += dx
    incremented_value = func(new_list)
    return (incremented_value - value_at_ansatz) / dx


def scalar_parabolic_minimize(scalar_func, ansatz_variable, value_at_ansatz, p_dt=1e-1):
    # Prediction from analytical parabola. E=a(t-t0)^2+E0
    if isclose(ansatz_variable, 0.0, abs_tol=1e-3):
        et = value_at_ansatz
    else:
        et = scalar_func(ansatz_variable)

    etp = scalar_func(ansatz_variable + p_dt)
    et_p = scalar_func(ansatz_variable - p_dt)

    a = (etp + et_p - 2 * et) / (2 * (p_dt ** 2))

    variable_diff = (etp - et_p) / (4 * p_dt * a)

    result_variable = ansatz_variable - variable_diff
    return result_variable


def parabolic_minimize(func, ansatz_list, value_at_ansatz, dx=1e-6, p_dt=1e-3):
    if len(ansatz_list) == 1:
        result_variable = scalar_parabolic_minimize(lambda var: func([var]), ansatz_list[0], value_at_ansatz)
        return func([result_variable]), [result_variable]

    derivative_vector = [
        get_partial_derivative(func, ansatz_list, value_at_ansatz, index, dx)
        for index in range(len(ansatz_list))
    ]

    def get_vector_norm(vector): return sum(map(lambda i: i ** 2, vector)) ** 0.5

    derivative_norm = get_vector_norm(derivative_vector)

    direction_vector = [partial_deriv / derivative_norm for partial_deriv in derivative_vector]

    def scalar_func(variable): return func([
        ansatz_variable - variable * derivative_vector_i
        for ansatz_variable, derivative_vector_i in zip(ansatz_list, direction_vector)
    ])

    result_variable = scalar_parabolic_minimize(scalar_func, 0.0, value_at_ansatz, p_dt)

    if abs(result_variable) > 0.05:
        result_variable = scalar_parabolic_minimize(scalar_func, result_variable, value_at_ansatz, p_dt)

    result_list = [
        ansatz_variable - result_variable * derivative_vector_i
        for ansatz_variable, derivative_vector_i in zip(ansatz_list, direction_vector)
    ]

    return scalar_func(result_variable), result_list
